//  Created by Matt Purchase.
//  Copyright (c) 2021 Matt Purchase. All rights reserved.
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UI_View {
	// Properties

	protected UI_ViewController m_controller;
	public CoreEvent e_viewInitialised;

	// Initalisation Functions

	public virtual void Initialise(UI_ViewController controller) {
		if (controller == null) {
			LogUtils.LogError("No controller detected");
		}
		m_controller = controller;
	}

	public virtual void Initialise(UI_ViewController controller, object data) {
		Initialise(controller);
	}

	public virtual void ShutDown() {


	}

	public virtual void HideView() {

	}

	public virtual void ShowView() {

	}



	// Public Functions

	// Private Functions

}