//  Created by Mathew Purchase.
//  Copyright (c) 2018 Mathew Purchase. All rights reserved.
using System;
using System.Collections.Generic;
// using Rewired;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class UI_ViewController : MonoBehaviour {

	// Properties
	[SerializeField] protected List<UI_ViewController> m_subPanelsToOpen;
	[SerializeField] protected List<UI_ViewController> m_subpanelsToClose;
	[SerializeField] protected GameObject m_firstSelected;


	protected Animator m_anim;



	protected UI_ViewController m_previousController;

	public bool m_isActive = false;
	public bool m_autoClose;
	[SerializeField] protected bool m_canGoBack = true;
	public bool m_canReturnTo = true;

	[SerializeField] private string m_name;

	// protected GKPanRecognizer m_recognizer;

	// public static Vector2Event e_panning;

	public CoreEvent e_viewClosed;
	public CoreEvent e_viewOpened;
	public static CoreEvent e_swipedLeft;
	public static CoreEvent e_swipedRight;
	public static CoreEvent e_swipedUp;
	public static CoreEvent e_swipedDown;
	public static ObjectEvent se_viewOpened;

	public bool m_darkUI = true;

	public bool m_swipesLeft = false;
	public bool m_swipesRight = false;
	public bool m_swipesUp = false;
	public bool m_swipesDown = false;

	protected bool m_pointerOnUIElement = false;



	// Init
	protected virtual void OnEnable() {
		// m_isActive = enabled;
		m_anim = GetComponent<Animator>();
	}

	protected virtual void OnDestroy() {
		UnsubscribeEvents();
	}

	protected virtual void OnDisable() {
		UnsubscribeEvents();
	}

	// Core Functions
	public void ToggleView(bool sure) {
		if (m_isActive) {
			Close();
		}
		else {
			Open();
		}
	}


	public string GetName() {
		if (string.IsNullOrWhiteSpace(m_name)) {
			return gameObject.name;
		}
		else {
			return m_name;
		}
	}


	public virtual void Close() {
		if (!m_isActive) {
			return;
		}

		m_isActive = false;
		if (m_anim != null) {
			m_anim.SetTrigger("Close");
		}
		else {
			gameObject.SetActive(false);
		}
		UnsubscribeEvents();
		CloseSubPanels();
		FireCloseEvent();
	}

	protected virtual void UnsubscribeEvents() {

		// if (m_recognizer != null) {
		// 	m_recognizer.gestureRecognizedEvent -= HandlePan;
		// 	m_recognizer.gestureCompleteEvent -= HandleSwipe;
		// }

		// PlayerInputHandler.Instance.e_platformChanged -= SetFirstSelected;
	}

	public void FireCloseEvent() {
		if (e_viewClosed != null) {
			e_viewClosed();
		}
	}


	public virtual void CloseSubPanels() {
		if (m_subPanelsToOpen.Count > 0) {
			for (int a = 0; a < m_subPanelsToOpen.Count; a++) {
				m_subPanelsToOpen[a].Close();
			}
		}
	}


	[ContextMenu("Close")]
	public virtual void ForceClose() {
		ForceClose(false);
	}


	public virtual void ForceClose(bool animates) {
		m_isActive = false;
		// if (m_recognizer != null) {
		// 	m_recognizer.gestureRecognizedEvent -= HandlePan;
		// 	m_recognizer.gestureCompleteEvent -= HandleSwipe;
		// }
		if (animates) {
			if (m_anim != null) {
				m_anim.SetTrigger("Close");
			}
			else {
				gameObject.SetActive(false);
			}
		}
		else {
			gameObject.SetActive(false);
		}

		// PlayerInputHandler.Instance.e_platformChanged -= SetFirstSelected;

		FireCloseEvent();
	}


	[ContextMenu("Open")]
	public virtual void Open() {
		if (m_isActive) {
			return;
		}

		if (m_anim == null) {
			m_anim = GetComponent<Animator>();
		}

		m_isActive = true;

		AutoGetFirstSelected();
		SetFirstSelected();

		if (m_anim != null) {
			gameObject.SetActive(true);
			m_anim.SetTrigger("Open");
		}
		else {

			gameObject.SetActive(true);
		}

		OpenPanels();
		ClosePanels();

		string screenName = m_name;
		if (string.IsNullOrWhiteSpace(screenName)) {
			screenName = gameObject.name;
		}

		// PlayerInputHandler.Instance.e_platformChanged += SetFirstSelected;

		SetupSwipes();

		// UI_ViewManager.Instance.RegisterAsMainController(this);

		if (e_viewOpened != null) {
			e_viewOpened();
		}
		if (se_viewOpened != null) {
			se_viewOpened(this);
		}
	}


	public void SetFirstSelected(object sender, EventArgs ea) {
		// LogUtils.Log(gameObject.name + " is setting selected");
		SetFirstSelected();
	}


	protected void SetFirstSelected() {
		if (m_firstSelected != null) {
			// UI_ViewManager.Instance.m_eventSystem.SetSelectedGameObject(m_firstSelected);
		}
	}


	public void SetSelected(GameObject selected) {
		// UI_ViewManager.Instance.m_eventSystem.SetSelectedGameObject(selected);
	}


	protected void AutoGetFirstSelected() {
		if (m_firstSelected != null) {
			return;
		}

		// TODO: make this recursive. Check for gameobject active in heirarchy
		foreach (Transform child in transform) {
			Selectable select = child.GetComponent<Selectable>();
			if (select != null) {
				m_firstSelected = select.gameObject;
				return;
			}
		}
	}


	protected virtual void Update() {
		SetSelectedOnNull();
		AndroidBackButtonHandler();
		DebugSwipes();
		CheckForUI();
	}


	protected virtual void CheckForUI() {
		if (UIUtils.CheckForUI()) {
			m_pointerOnUIElement = true;
		}
		else {
			m_pointerOnUIElement = false;
		}
	}


	private void SetSelectedOnNull() {

		// if (UI_ViewManager.Instance == null) {
		// 	return;
		// }

		// if (UI_ViewManager.Instance.m_eventSystem.currentSelectedGameObject == null) {
		// 	SetFirstSelected();
		// 	return;
		// }

		// if (!UI_ViewManager.Instance.m_eventSystem.currentSelectedGameObject.activeInHierarchy) {
		// 	SetFirstSelected();
		// }
	}


	protected virtual void AndroidBackButtonHandler() {
		if (!m_isActive) { return; }
		if (m_previousController == null) { return; }
		if (m_previousController == this) { return; }
		if (!m_previousController.m_canReturnTo) { return; }
		if (!m_canGoBack) { return; }

		// if (Input.GetButtonDown("menu")) {
		// 	Back();
		// }

		// if (PlayerInputHandler.m_player.GetButtonDown("menu")) {
		// 	Back();
		// }
		// if (PlayerInputHandler.m_player.GetButtonDown("back")) {
		// 	Back();
		// }
	}


	public virtual void Back() {
		if (m_previousController != null && m_previousController != this) {
			Segue(m_previousController);
		}
	}


	private void ClosePanels() {
		if (m_subpanelsToClose.Count > 0) {
			for (int a = 0; a < m_subpanelsToClose.Count; a++) {
				if (m_subpanelsToClose[a] != null)
					m_subpanelsToClose[a].ForceClose(false);
			}
		}
	}


	private void OpenPanels() {
		if (m_subPanelsToOpen.Count > 0) {
			for (int a = 0; a < m_subPanelsToOpen.Count; a++) {
				if (m_subPanelsToOpen[a] != null)
					m_subPanelsToOpen[a].Open();
			}
		}
	}


	public virtual void Segue(UI_ViewController to) {
		if (m_isActive) {
			Close();
			to.SetPrevious(this);
			to.Open();
		}
		else {
			LogUtils.LogError("can't segue, " + gameObject + " is not active");
		}

	}


	public void SetPrevious(UI_ViewController ctrl) {
		m_previousController = ctrl;
	}


	public virtual void LoadData(object data) {

	}



	private void SetupSwipes() {
		bool willSetup = false;
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			willSetup = true;
		}
		if (Application.platform == RuntimePlatform.Android) {
			willSetup = true;
		}
		if (Application.platform == RuntimePlatform.OSXEditor) {
			willSetup = true;
		}

		if (!willSetup) {
			return;
		}

		// m_recognizer = new GKPanRecognizer();
		// // m_recognizer.swipesToDetect = GKSwipeDirection.All;
		// m_recognizer.gestureRecognizedEvent += HandlePan;
		// m_recognizer.gestureCompleteEvent += HandleSwipe;
		// GestureKit.addGestureRecognizer(m_recognizer);
	}



	private void DebugSwipes() {


		// // inverted because swipes are inverted.
		// if (Input.GetKeyDown(KeyCode.RightArrow) || PlayerInputHandler.m_player.GetButtonDown("menu_right")) {
		// 	SwipeLeft();
		// }
		// if (Input.GetKeyDown(KeyCode.LeftArrow) || PlayerInputHandler.m_player.GetButtonDown("menu_left")) {
		// 	SwipeRight();
		// }
		// if (Input.GetKeyDown(KeyCode.DownArrow) || PlayerInputHandler.m_player.GetButtonDown("menu_down")) {
		// 	SwipeUp();
		// }
		// if (Input.GetKeyDown(KeyCode.UpArrow) || PlayerInputHandler.m_player.GetButtonDown("menu_up")) {
		// 	SwipeDown();
		// }

	}


	// protected virtual void HandlePan(GKPanRecognizer pan) {

	// 	if (PlatformOverUI()) {
	// 		return;
	// 	}

	// 	if (!HasSwipes()) {
	// 		return;
	// 	}

	// 	Vector2 dir = pan.deltaTranslation;

	// 	if (!m_swipesDown && dir.y < 0) {
	// 		dir.y = 0;
	// 	}
	// 	if (!m_swipesUp && dir.y > 0) {
	// 		dir.y = 0;
	// 	}
	// 	if (!m_swipesLeft && dir.x < 0) {
	// 		dir.x = 0;
	// 	}
	// 	if (!m_swipesRight && dir.x < 0) {
	// 		dir.x = 0;
	// 	}

	// 	float xm = 0;
	// 	float ym = 0;
	// 	bool horizontal = DetermineHorizontal(dir.x, dir.y, out xm, out ym);

	// 	if (horizontal) {
	// 		dir.y = 0;
	// 	}
	// 	else {
	// 		dir.x = 0;
	// 	}


	// 	if (e_panning != null) {
	// 		e_panning(dir);
	// 	}
	// }




	// protected void HandleSwipe(GKPanRecognizer pan) {


	// 	bool right = false;
	// 	bool left = false;
	// 	bool up = false;
	// 	bool down = false;



	// 	Vector2 dir = pan.totalDeltaTranslation;

	// 	// determine gross direction first.
	// 	float x = dir.x;
	// 	float y = dir.y;
	// 	float xMag = 0;
	// 	float yMag = 0;


	// 	bool horizontal = DetermineHorizontal(x, y, out xMag, out yMag);

	// 	if (horizontal) {
	// 		if (m_swipesRight) {
	// 			if (dir.x > 0) {
	// 				right = true;
	// 			}
	// 		}
	// 		if (m_swipesLeft) {
	// 			if (dir.x < 0) {
	// 				left = true;
	// 			}
	// 		}
	// 	}
	// 	else {
	// 		if (m_swipesUp) {
	// 			if (dir.y > 0) {
	// 				up = true;
	// 			}
	// 		}
	// 		if (m_swipesDown) {
	// 			if (dir.y < 0) {
	// 				down = true;
	// 			}
	// 		}
	// 	}

	// 	bool xMagAchieved = false;
	// 	bool yMagAchieved = false;

	// 	if (xMag > 0.3f) {
	// 		xMagAchieved = true;
	// 	}

	// 	if (yMag > 0.2f) {
	// 		yMagAchieved = true;
	// 	}

	// 	if (right && xMagAchieved) {
	// 		SwipeRight();
	// 	}
	// 	if (left && xMagAchieved) {
	// 		SwipeLeft();
	// 	}
	// 	if (up && yMagAchieved) {
	// 		SwipeUp();
	// 	}
	// 	if (down && yMagAchieved) {
	// 		SwipeDown();
	// 	}
	// }


	private bool DetermineHorizontal(float x, float y, out float xMag, out float yMag) {
		if (x < 0) {
			x *= -1;
		}
		if (y < 0) {
			y *= -1;
		}

		xMag = x / Screen.width;
		yMag = y / Screen.height;

		if (x > y) {
			return true;
		}
		return false;
	}


	protected virtual void SwipeLeft() {
		if (PlatformOverUI()) {
			return;
		}

		if (m_swipesLeft) {
			if (e_swipedLeft != null) {
				e_swipedLeft();
			}
		}
	}


	protected virtual void SwipeRight() {
		if (PlatformOverUI()) {
			return;
		}

		if (m_swipesRight) {
			if (e_swipedRight != null) {
				e_swipedRight();
			}
		}
	}


	protected virtual void SwipeUp() {

		if (PlatformOverUI()) {
			return;
		}

		if (m_swipesUp) {
			if (e_swipedUp != null) {
				e_swipedUp();
			}
		}
	}


	protected virtual void SwipeDown() {
		if (PlatformOverUI()) {
			return;
		}
		if (m_swipesDown) {
			if (e_swipedDown != null) {
				e_swipedDown();
			}
		}
	}


	private bool PlatformOverUI() {
		// if (PlayerInputHandler.Instance.m_currentPlatform == n_platformType.mobile) {
		// 	if (m_pointerOnUIElement) {
		// 		return true;
		// 	}
		// }
		return false;
	}


	private bool HasSwipes() {
		bool swipes = false;
		if (m_swipesDown) { swipes = true; }
		if (m_swipesUp) { swipes = true; }
		if (m_swipesLeft) { swipes = true; }
		if (m_swipesRight) { swipes = true; }
		return swipes;
	}

}
