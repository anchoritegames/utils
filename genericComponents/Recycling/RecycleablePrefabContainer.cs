//  Created by Matt Purchase.
//  Copyright (c) 2021 Matt Purchase. All rights reserved.
using UnityEngine;
using System;
using System.Collections.Generic;


[Serializable]
public class RecycleablePrefabContainer {
	// Properties
	[SerializeField] private string m_name;
	public List<Recyclable> m_spawnedItems;
	public List<Recyclable> m_inactiveItems;
	public List<Recyclable> m_activeItems;
	public Recyclable m_spawnablePrefab;


	// Initalisation Functions
	public RecycleablePrefabContainer(GameObject prefab) {
		m_spawnedItems = new List<Recyclable>();
		m_activeItems = new List<Recyclable>();
		m_inactiveItems = new List<Recyclable>();

		Recyclable cylceable = prefab.GetComponent<Recyclable>();
		if (cylceable == null) {
			LogUtils.LogError("Tried to create a container from a non recycleable pefab: " + prefab);
		}
		m_spawnablePrefab = cylceable;
		m_name = prefab.name;
	}

	// Public Functions
	public Recyclable GetActiveItem(GameObject prefab) {

		if (m_inactiveItems.Count > 0) {
			Recyclable item = m_inactiveItems[0];
			m_inactiveItems.Remove(item);

			m_activeItems.Add(item);
			item.gameObject.SetActive(true);
			return item;
		}

		GameObject newItem = ObjectUtils.SpawnRaw(prefab);

		Recyclable recycle = newItem.GetComponent<Recyclable>();
		if (recycle == null) {
			LogUtils.LogError("Item was not recycleable");
			return null;
		}

		m_spawnedItems.Add(recycle);
		m_activeItems.Add(recycle);
		return recycle;
	}

	public void DeactivateRecycleable(GameObject prefab) {

		Recyclable cycle = prefab.GetComponent<Recyclable>();

		if (m_activeItems.Contains(cycle)) {
			m_activeItems.Remove(cycle);
		}

		m_inactiveItems.Add(cycle);

		prefab.gameObject.SetActive(false);
	}

	public void ForceClearSpawnedItem(GameObject prefab) {
		Recyclable cycle = prefab.GetComponent<Recyclable>();
		m_activeItems.Remove(cycle);
		m_inactiveItems.Remove(cycle);
		m_spawnedItems.Remove(cycle);
		GameObject.DestroyImmediate(prefab);
	}

	public bool MatchesPrefab(GameObject obj) {
		if (obj == m_spawnablePrefab.gameObject) {
			return true;
		}
		return false;
	}

	public bool MatchesSpawnedItem(GameObject obj) {
		return m_spawnedItems.Contains(obj.GetComponent<Recyclable>());
	}

	// Private Functions

}