//  Created by Matt Purchase.
//  Copyright (c) 2021 Matt Purchase. All rights reserved.
using System;
using System.Collections.Generic;
using UnityEngine;


public class Recyclable : MonoBehaviour {
	// Properties
	protected object m_data;
	protected object m_manager;

	public CoreEvent e_created;
	public CoreEvent e_initialised;
	public CoreEvent e_deactivated;

	// Initalisation Functions

	// Unity Callbacks

	protected virtual void OnEnable() {
		if (e_created != null) {
			e_created();
		}
	}
	// Public Functions
	public virtual void Initialise(object manager, object data) {
		m_manager = manager;
		m_data = data;
		Initialise();
	}

	public virtual void Initialise() {

		if (e_initialised != null) {
			e_initialised();
		}
	}
	// Private Functions

	public virtual void Deactivate() {
		if (e_deactivated != null) {
			e_deactivated();
		}
		gameObject.SetActive(false);
	}

}