//  Created by Matt Purchase.
//  Copyright (c) 2021 Matt Purchase. All rights reserved.
using System;
using System.Collections.Generic;
using UnityEngine;

// The main reason this is a monobehaviour is so that it's quickly inspectable. It'll probably be worthwhile building it's own independent inspector with extra info and turning it into a static class.

public class ObjectRecycler : MonoBehaviour {
	// Properties
	public List<RecycleablePrefabContainer> m_registeredPrefabs;

	private static ObjectRecycler m_instance;
	public static ObjectRecycler Instance {
		get {

			if (m_instance == null) {
				m_instance = FindObjectOfType<ObjectRecycler>();
			}

			if (m_instance == null) {
				GameObject recycler = new GameObject();
				recycler.name = "Object Recycler";
				ObjectRecycler cycle = recycler.AddComponent<ObjectRecycler>();
				cycle.Initialise();
				m_instance = cycle;
			}

			return m_instance;
		}
	}

	public ObjectEvent e_itemSpawned;

	// Initalisation Functions
	public void Initialise() {
		m_registeredPrefabs = new List<RecycleablePrefabContainer>();
	}

	// Unity Callbacks

	// Public Functions


	public void DespawnItem(GameObject prefab) {
		bool success = false;
		foreach (RecycleablePrefabContainer recycle in m_registeredPrefabs) {
			if (recycle.MatchesSpawnedItem(prefab)) {
				recycle.DeactivateRecycleable(prefab);
				success = true;
			}
		}

		if (!success) {
			LogUtils.LogError("Failed to deactivate recycleable");
		}
	}

	public void ForceDestroyItem(GameObject prefab) {
		// bool success = false;
		foreach (RecycleablePrefabContainer recycle in m_registeredPrefabs) {
			if (recycle.MatchesSpawnedItem(prefab)) {
				recycle.ForceClearSpawnedItem(prefab);
				// success = true;
			}
		}
		// if (!success) {
		// 	LogUtils.LogError("Failed to destroy recycleable");
		// }
	}

	public GameObject SpawnItem(GameObject prefab) {
		return SpawnItem(prefab, null, null);
	}

	public GameObject SpawnItem(GameObject prefab, object data, object manager) {

		Recyclable recyclable = GetItem(prefab);

		if (recyclable == null) {
			LogUtils.LogError("item is not recyclable: " + prefab);
			return null;
		}

		if (e_itemSpawned != null) {
			e_itemSpawned(recyclable);
		}
		recyclable.Initialise(manager, data);

		return recyclable.gameObject;
	}

	// Private Functions
	private Recyclable GetItem(GameObject prefab) {
		foreach (RecycleablePrefabContainer recycle in m_registeredPrefabs) {
			if (recycle.MatchesPrefab(prefab)) {
				return recycle.GetActiveItem(prefab);
			}
		}

		// we assume no recycler exists for this?
		RecycleablePrefabContainer container = CreateContainer(prefab);
		return container.GetActiveItem(prefab);
	}

	private RecycleablePrefabContainer CreateContainer(GameObject prefab) {
		RecycleablePrefabContainer container = new RecycleablePrefabContainer(prefab);
		m_registeredPrefabs.Add(container);
		return container;
	}

}