//  Created by Matt Purchase.
//  Copyright (c) 2021 Matt Purchase. All rights reserved.
using System;
using System.Collections.Generic;
using UnityEngine;


public class Draggable : MonoBehaviour {

	private bool m_mouseStateDown;
	private GameObject m_target;
	private Vector3 m_screenSpace;
	private Vector3 m_offset;
	private float m_yOffset = 0;
	private bool m_canDrag = true;
	private Rigidbody _body;
	public Rigidbody m_body {
		get {
			if (_body == null) {
				_body = GetComponent<Rigidbody>();
			}
			if (_body == null) {
				_body = gameObject.AddComponent<Rigidbody>();
			}
			return _body;
		}
	}


	public CoreEvent e_letGo;
	// Initalisation Functions
	private void OnEnable() {

	}

	// Unity Callbacks

	private void LateUpdate() {

		if (!m_canDrag) {
			return;
		}

		if (Input.GetMouseButtonDown(0)) {

			RaycastHit hitInfo;
			m_target = ObjectUtils.GetClickedObject(out hitInfo);
			if (m_target != gameObject) {
				return;
			}
			if (m_target != null) {
				m_mouseStateDown = true;
				m_screenSpace = Camera.main.WorldToScreenPoint(m_target.transform.position);
				m_offset = m_target.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, m_screenSpace.z));
				m_yOffset = m_target.transform.position.y;
			}
		}
		if (Input.GetMouseButtonUp(0)) {
			LetGo();
		}
		if (m_mouseStateDown) {
			//keep track of the mouse position
			var curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, m_screenSpace.z);

			//convert the screen mouse position to world point and adjust with offset
			var curPosition = Camera.main.ScreenToWorldPoint(curScreenSpace) + m_offset;
			curPosition.y = m_yOffset;

			//update the position of the object in the world
			m_target.transform.position = curPosition;

		}
	}

	private void OnDisable() {
		LetGo();
	}

	// Public Functions

	public void Enable() {

		m_body.isKinematic = false;
		m_canDrag = true;
	}

	public void Disable() {
		m_canDrag = false;
	}

	public void LetGo() {
		m_mouseStateDown = false;
		if (e_letGo != null) {
			e_letGo();
		}
	}

	// Private Functions

}