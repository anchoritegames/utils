//  Created by Matt Purchase.
//  Copyright (c) 2020 Matt Purchase. All rights reserved.
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public static class UIUtils {
	// Public Functions
	public static void SubscribeButton(Button butt, UnityEngine.Events.UnityAction function) {
		butt.onClick.RemoveAllListeners();
		butt.onClick.AddListener(function);
	}

	public static void SubscribeInput(InputField field, UnityEngine.Events.UnityAction<string> function) {
		field.onEndEdit.RemoveAllListeners();
		field.onEndEdit.AddListener(function);
	}

	public static bool CheckForUI() {
		if (EventSystem.current.IsPointerOverGameObject(0)) {
			return true;
		}
		if (EventSystem.current.IsPointerOverGameObject(-1)) {
			return true;
		}
		if (EventSystem.current.currentSelectedGameObject != null) {
			return true;
		}

		return false;
	}

}