//  Created by Matt Purchase.
//  Copyright (c) 2020 Matt Purchase. All rights reserved.
using System;
using System.Collections.Generic;

public delegate void CoreEvent();
public delegate void ObjectEvent(object obj);
public delegate void IntEvent(int obj);
public delegate void FloatEvent(float obj);
public delegate void BoolEvent(bool obj);
public delegate void StringEvent(string obj);