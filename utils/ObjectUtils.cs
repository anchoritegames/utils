//  Created by Matt Purchase.
//  Copyright (c) 2020 Matt Purchase. All rights reserved.
using System;
using System.Collections.Generic;
using UnityEngine;

public static class ObjectUtils {

	public static GameObject Spawn(GameObject prefab, GameObject parent) {
		GameObject obj = ObjectRecycler.Instance.SpawnItem(prefab);
		Parent(parent, obj);
		return obj;
	}

	public static GameObject SpawnUI(GameObject prefab, GameObject parent) {
		GameObject obj = ObjectRecycler.Instance.SpawnItem(prefab);
		Parent(parent, obj, false);
		obj.transform.position = Vector3.zero;
		return obj;
	}

	public static GameObject SpawnData(GameObject prefab, GameObject parent, object manager, object data) {
		GameObject obj = ObjectRecycler.Instance.SpawnItem(prefab, data, manager);
		Parent(parent, obj);
		return obj;
	}

	public static GameObject SpawnUIData(GameObject prefab, GameObject parent, object manager, object data) {
		GameObject obj = ObjectRecycler.Instance.SpawnItem(prefab, data, manager);
		Parent(parent, obj, false);
		obj.transform.position = Vector3.zero;
		return obj;
	}

	private static void Parent(GameObject parent, GameObject obj, bool ui = true) {
		if (parent != null) {
			obj.transform.SetParent(parent.transform, ui);
		}
		else {
			obj.transform.SetParent(null);
		}
	}

	public static void DespawnItem(GameObject prefab) {
		ObjectRecycler.Instance.DespawnItem(prefab);
	}

	public static void ForceDestroyItem(GameObject prefab) {
		ObjectRecycler.Instance.ForceDestroyItem(prefab);
	}

	public static GameObject SpawnRaw(GameObject prefab) {
		return GameObject.Instantiate(prefab);
	}


	public static GameObject GetClickedObject(out RaycastHit hit) {
		GameObject target = null;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(ray.origin, ray.direction * 10, out hit)) {
			target = hit.collider.gameObject;
		}

		return target;
	}

	public static void SetPosAndRot(GameObject spawnedItem, Vector3 pos, Quaternion rotation) {
		spawnedItem.transform.position = pos;
		spawnedItem.transform.rotation = rotation;
	}

	public static string TrimSpawnName(string objectName) {
		string trimmedName = "";
		if (objectName.Contains("(Clone)")) {
			string[] newname = objectName.Split('(');
			trimmedName = newname[0];
		}
		else {
			trimmedName = objectName;
		}
		return trimmedName;
	}


	public static Component InitComponent<T>(GameObject inputObject) where T : Component {
		Component obj = inputObject.GetComponent<T>();
		if (obj == null) {
			obj = inputObject.AddComponent<T>();
		}
		return obj;
	}


	public static void DropItemAroundTransform(GameObject item, Transform trans, float range = 3.0f) {
		// set its pos
		Vector2 cir = UnityEngine.Random.insideUnitCircle * range;
		Vector3 pos = trans.position;
		pos.x += cir.x;
		pos.z += cir.y;
		item.transform.position = pos;
	}

}